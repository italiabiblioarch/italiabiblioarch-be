import mongoose from 'mongoose';

const Paper = mongoose.model(
  'Paper',
  mongoose.Schema({
    authors: mongoose.Schema.Types.String,
    year: mongoose.Schema.Types.Number,
    title: { type: mongoose.Schema.Types.String, unique: true },
    journal_book: mongoose.Schema.Types.String,
    doi: mongoose.Schema.Types.String,
    regions: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Region',
      },
    ],
    chronology: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Chronology',
      },
    ],
    keywords: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Keyword',
      },
    ],
    abstract: mongoose.Schema.Types.String,
  })
);

const Author = mongoose.model(
  'Author',
  mongoose.Schema({
    name: { type: mongoose.Schema.Types.String, unique: true },
  })
);

const Keyword = mongoose.model(
  'Keyword',
  mongoose.Schema({
    name: { type: mongoose.Schema.Types.String, unique: true },
  })
);

const Chronology = mongoose.model(
  'Chronology',
  mongoose.Schema({
    name: { type: mongoose.Schema.Types.String, unique: true },
  })
);

const Region = mongoose.model(
  'Region',
  mongoose.Schema({
    name: { type: mongoose.Schema.Types.String, unique: true },
  })
);

export default {
  Paper,
  Author,
  Keyword,
  Chronology,
  Region,
};
