import dbConnect from '../../schema/db';
import Model from '../../schema/model';
import allowCors from '../allowCors';

const handler = async (_, res) => {
  const { Chronology, Keyword, Region } = Model;
  const db = await dbConnect();
  try {
    const chronology = await Chronology.find().exec();
    const keywords = await Keyword.find().exec();
    const regions = await Region.find().exec();

    res.status(200).send({ chronology, keywords, regions });
  } catch (e) {
    res.status(400).send({ error: `ERROR: ${e}` });
  }

  await db.connection.close();
};

module.exports = allowCors(handler);
